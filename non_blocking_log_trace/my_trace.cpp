#include "my_trace.h"

my_trace::my_trace() 
{
	_tcp.set_ip("127.0.0.1");
	_tcp.set_port(8888);
	_tcp.open();
	_ser.open("COM10", serial::SPEED_9600);
	_myfile.open("log.log");
}

my_trace::~my_trace() 
{
	_tcp.close();
	_ser.close();
	_myfile.close();
}

void my_trace::send_log(string msg) 
{
	send_log_to_tcp(msg);
	send_log_to_com(msg);
	send_log_to_file(msg);
	std::cout <<msg << endl;
}
