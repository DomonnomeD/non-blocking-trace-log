#pragma once

#include "trace.h"
#include "tcpClient.h"
#include "serial.h"
#include <fstream>

class my_trace : public trace 
{
public:
	my_trace();
	~my_trace();
	void send_log(string msg) override; 

private:
	ofstream _myfile;
	serial _ser;
	tcpClient _tcp;

	void send_log_to_tcp(string msg) {
		_tcp.write(msg);
	}
	void send_log_to_com(string msg) {
		_ser.write(msg);
	}
	void send_log_to_file(string msg) {
		_myfile << msg;
	}
};

