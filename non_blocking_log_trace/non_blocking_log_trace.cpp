﻿#include <iostream>
#include "trace.h"
#include "my_trace.h"
#include "trace_cout.h"
#include "tcpClient.h"

int main()
{
    tcpClient tcp_test("127.0.0.1",8888);
    tcp_test.open();
    tcp_test.write("test tcp");
    tcp_test.close();

    serial serial_test;
    serial_test.open("COM10", serial::SPEED_9600);
    serial_test.write("test serial");
    serial_test.close();

    /*
     * Klase paveldeta is "trace" klases, joje realizuojama kaip
     * reikia isvesti logus (pagal uzduoti i tris vietas).
     * Jei reikia kazko papildomo realizuoti kitoje platformoje,
     * paveldim ir realizuojam koda taip, kad veiktu ant pasirinktos platformos
    */
    my_trace my_trace_test;
    my_trace_test.add_logLevel(my_trace::SET_DEBUG_LEVEL_ERROR);
    my_trace_test.log(my_trace::DEBUG_LEVEL_ERROR, "test my_trace");
    my_trace_test.log_error("test my_trace2");
    my_trace_test.log_warning("test my_trace3");

    //Bazine klase, joje yra kaupiami logai ir isvedami i std::cout
    trace trace_test;
    trace_test.add_logLevel(trace::SET_DEBUG_LEVEL_ERROR);
    trace_test.log(trace::DEBUG_LEVEL_ERROR,"test trace");
    trace_test.log_error("test test2");
    trace_test.log_warning("test test3");

    return 0;
}