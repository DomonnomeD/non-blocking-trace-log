#include "serial.h"

serial::serial() 
{
    DCB dcbSerialParams = { 0 };
    COMMTIMEOUTS timeouts = { 0 };
}

serial::~serial() {};

bool serial::open(std::string com_port, SERIAL_SPEED speed) 
{
    std::string str = "\\\\.\\"+ com_port;
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    std::wstring wide = converter.from_bytes(str);
    LPCWSTR com = wide.c_str();
    _hSerial = CreateFile(com, GENERIC_READ | GENERIC_WRITE, 0, NULL,
                                OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    if(_hSerial == INVALID_HANDLE_VALUE)
        return 1;

    _dcbSerialParams.DCBlength = sizeof(_dcbSerialParams);
    if(GetCommState(_hSerial, &_dcbSerialParams) == 0)
    {
        CloseHandle(_hSerial);
        return 1;
    }

    _dcbSerialParams.BaudRate = speed;
    _dcbSerialParams.ByteSize = 8;
    _dcbSerialParams.StopBits = ONESTOPBIT;
    _dcbSerialParams.Parity = NOPARITY;
    if (SetCommState(_hSerial, &_dcbSerialParams) == 0)
    {
        CloseHandle(_hSerial);
        return 1;
    }

    _timeouts.ReadIntervalTimeout = 50;
    _timeouts.ReadTotalTimeoutConstant = 50;
    _timeouts.ReadTotalTimeoutMultiplier = 10;
    _timeouts.WriteTotalTimeoutConstant = 50;
    _timeouts.WriteTotalTimeoutMultiplier = 10;
    if (SetCommTimeouts(_hSerial, &_timeouts) == 0)
    {
        CloseHandle(_hSerial);
        return 1;
    }

    return 0;
}

bool serial::write(std::string msg)
{
    DWORD bytes_written, total_bytes_written = 0;
    if (!WriteFile(_hSerial, msg.c_str(), msg.size(), &bytes_written, NULL))
        return 1;
}

bool serial::close() 
{
    if (CloseHandle(_hSerial) == 0)
        return 1;
    return 0;
}