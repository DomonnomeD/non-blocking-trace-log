#pragma once

#include <windows.h>
#include <codecvt>
#include <iostream>

class serial
{
public:
	enum SERIAL_SPEED {
		SPEED_110 = 110,
		SPEED_300 = 300,
		SPEED_600 = 600,
		SPEED_1200 = 1200,
		SPEED_2400 = 2400,
		SPEED_4800 = 4800,
		SPEED_9600 = 9600,
		SPEED_14400 = 14400,
		SPEED_19200 = 19200,
		SPEED_38400 = 38400,
		SPEED_56000 = 56000,
		SPEED_57600 = 57600,
		SPEED_115200 = 115200,
		SPEED_128000 = 128000,
		SPEED_256000 = 256000
	};

    serial();
    ~serial();
    bool open(std::string com_port,SERIAL_SPEED speed);
    bool write(std::string msg);
    bool close();

private:
    HANDLE _hSerial;
    DCB _dcbSerialParams;
    COMMTIMEOUTS _timeouts;
};

