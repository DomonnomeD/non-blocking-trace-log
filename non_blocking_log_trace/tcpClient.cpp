#include "tcpClient.h"

tcpClient::tcpClient() {};

tcpClient::tcpClient(std::string ip, int port)
{
	tcpClient::_ip = ip;
	tcpClient::_port = port;
};

tcpClient::~tcpClient() {};

bool tcpClient::open()
{
	int iResult;
	WSADATA wsaData;

	struct sockaddr_in clientService;

	//----------------------
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		std::cout << "WSAStartup failed with error " << iResult << std::endl;
		return 1;
	}

	//----------------------
	// Create a SOCKET for connecting to server
	_ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_ConnectSocket == INVALID_SOCKET) {
		std::cout << "socket failed with error: " << WSAGetLastError() << std::endl;
		WSACleanup();
		return 1;
	}

	//----------------------
	// The sockaddr_in structure specifies the address family,
	// IP address, and port of the server to be connected to.
	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr(_ip.c_str());
	clientService.sin_port = htons(_port);

	//----------------------
	// Connect to server.
	iResult = connect(_ConnectSocket, (SOCKADDR*)&clientService, sizeof(clientService));
	if (iResult == SOCKET_ERROR) {
		std::cout << "connect failed with error:  << WSAGetLastError() " << std::endl;
		closesocket(_ConnectSocket);
		WSACleanup();
		return 1;
	}
	return 0;
}

void tcpClient::close() {
	closesocket(_ConnectSocket);
	WSACleanup();
}

bool tcpClient::write(std::string msg) {
	int iResult = send(_ConnectSocket, msg.c_str(), msg.size(), 0);
	if (iResult == SOCKET_ERROR) {
		std::cout << "send failed with error : " << WSAGetLastError() << std::endl;
		closesocket(_ConnectSocket);
		WSACleanup();
		return 1;
	}
	return 0;
}

std::string tcpClient::read() {
	int iResult = 1;
	const int size = 512;
	char recv_tmp[size] = "";
	iResult = recv(_ConnectSocket, recv_tmp, size, 0);
	if (iResult > 0) {
		return recv_tmp;
	}
	return "";
}

void tcpClient::set_ip(std::string ip)
{
	tcpClient::_ip = ip;
}

void tcpClient::set_port(int port)
{
	tcpClient::_port = port;
}