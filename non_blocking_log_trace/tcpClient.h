#pragma once

#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
#include <string>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>

// Need to link with Ws2_32.lib
#pragma comment(lib, "ws2_32.lib")

class tcpClient
{
public:
    tcpClient();
    tcpClient(std::string ip, int port);
    ~tcpClient();
    void set_ip(std::string ip);
    void set_port(int port);
    bool open();
    void close();
    bool write(std::string msg);
    std::string read();

private:
    SOCKET _ConnectSocket;
    std::string _ip;
    int _port;
};

