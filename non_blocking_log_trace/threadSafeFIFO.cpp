#pragma once

#include "threadSafeFIFO.h"

template <typename T>
threadSafeFIFO<T>::threadSafeFIFO() {}

template <typename T>
threadSafeFIFO<T>::~threadSafeFIFO() {}

template <typename T>
void threadSafeFIFO<T>::push(T&& data)
{
	std::lock_guard<std::mutex> lg(_mutex);
	_deque.push_back(std::move(data));
}

template <typename T>
void threadSafeFIFO<T>::push(const T& data)
{
	std::lock_guard<std::mutex> lg(_mutex);
	_deque.push_back(data);
}

template <typename T>
T threadSafeFIFO<T>::pop()
{
	std::lock_guard<std::mutex> lg(_mutex);
	T deque_tmp;

	if (_deque.size() != 0) {
		deque_tmp = _deque.front();
		_deque.pop_front();
	}

	return deque_tmp;
}

template <typename T>
bool threadSafeFIFO<T>::isEmpty()
{
	std::lock_guard<std::mutex> lg(_mutex);
	return ((_deque.size() == 0)) ? true : false;
}