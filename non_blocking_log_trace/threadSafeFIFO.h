#pragma once

#include <queue>
#include <mutex>
#include <iostream>

template <typename T>
class threadSafeFIFO
{
public:
	threadSafeFIFO();
	~threadSafeFIFO();
	void push(T&& data);
	void push(const T& data);
	T pop();
	bool isEmpty();

private:
	std::deque<T> _deque;
	std::mutex _mutex;
};
