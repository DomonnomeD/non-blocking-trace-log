#include "trace.h"

trace::trace()
{
	_threadIsAlive = true;
	_killThread = false;
	_setupLogLevels = { SET_DEBUG_LEVEL_DISABLED };

	thread t1(&trace::thread_TraceSender, this);
	t1.detach();
}

trace::~trace()
{
	_killThread = true;
	while (_threadIsAlive);
}

void trace::thread_TraceSender()
{
	while (_killThread && !_safeFIFO.isEmpty())
		trace::send_log(_safeFIFO.pop());
	_threadIsAlive = false;
}

void trace::log(debugLevel level, string msg)
{
	if (std::find(_setupLogLevels.begin(), _setupLogLevels.end(), level) != _setupLogLevels.end()) {
		_safeFIFO.push("[" + _log_levels_string[level] + "]" + " " + msg);
	}
}

void trace::remove_logLevel(setUpDebugLevel level)
{
	_setupLogLevels.erase(std::remove(_setupLogLevels.begin(), _setupLogLevels.end(), level), _setupLogLevels.end());
}

void trace::add_logLevel(setUpDebugLevel level)
{
	if (level == SET_DEBUG_LEVEL_DISABLED)
	{
		_setupLogLevels.clear();
		_setupLogLevels.push_back(level);
	}
	if (std::find(_setupLogLevels.begin(), _setupLogLevels.end(), level) == _setupLogLevels.end())
	{
		_setupLogLevels.erase(std::remove(_setupLogLevels.begin(), _setupLogLevels.end(), SET_DEBUG_LEVEL_DISABLED), _setupLogLevels.end());
		_setupLogLevels.push_back(level);
	}
}

void trace::send_log(string msg)
{
	std::cout << msg << endl;
};

void trace::log_error(string msg)
{
	trace::log(trace::DEBUG_LEVEL_ERROR, msg);
};

void trace::log_warning(string msg)
{
	trace::log(trace::DEBUG_LEVEL_WARNING, msg);
};

void trace::log_message(string msg)
{
	trace::log(trace::DEBUG_LEVEL_MESSAGE, msg);
};