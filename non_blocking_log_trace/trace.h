#pragma once

#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <vector>
#include "threadSafeFIFO.cpp"  //Method 2: https://www.codeproject.com/Articles/48575/How-to-Define-a-Template-Class-in-a-h-File-and-Imp

using namespace std;

class trace
{
public:
	enum setUpDebugLevel
	{
		SET_DEBUG_LEVEL_DISABLED = 999,
		SET_DEBUG_LEVEL_ERROR = 0,
		SET_DEBUG_LEVEL_WARNING = 1,
		SET_DEBUG_LEVEL_MESSAGE = 2,
	};

	enum debugLevel
	{
		DEBUG_LEVEL_ERROR = 0,
		DEBUG_LEVEL_WARNING = 1,
		DEBUG_LEVEL_MESSAGE = 2,
	};

	trace();
	~trace();
	void log(debugLevel level, string msg);
	void log_error(string msg);
	void log_warning(string msg);
	void log_message(string msg);
	void add_logLevel(setUpDebugLevel level);
	void remove_logLevel(setUpDebugLevel level);
	virtual void send_log(string msg);

private:
	bool _threadIsAlive;
	bool _killThread;
	threadSafeFIFO<std::string> _safeFIFO;
	const string _log_levels_string[3] = {"ERROR","WARNING","MESSAGE"};
	vector<setUpDebugLevel> _setupLogLevels;
	void thread_TraceSender();
};


