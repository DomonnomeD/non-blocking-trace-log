#pragma once

#include "trace.h"
#include <iostream>

class trace_cout : public trace
{
public:
	void send_log(string msg) override;
};